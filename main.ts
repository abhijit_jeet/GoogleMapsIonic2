import { Geolocation } from '@ionic-native/geolocation';
import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 LatLng,
 CameraPosition,
 MarkerOptions,
 Marker
} from '@ionic-native/google-maps';

@Component({
  selector: 'main',
  templateUrl: 'main.html'
})
export class mainPage {
    marker: any;
    map: GoogleMap;
    latLng: any;
   constructor(private platform: Platform, public navCtrl: NavController, public navParams: NavParams, private geoLocation: Geolocation) {
     //Get location when page loads
     platform.ready().then(() => {
      this.getCurrentPosition();
    });

   }
   //Get location of the device
   getCurrentPosition(){
     this.geoLocation.getCurrentPosition().then(position => {
       let lat = position.coords.latitude;
       let lng = position.coords.longitude;

       this.latLng = new LatLng(lat,lng)
       //loads map after getting the coordinates
       this.loadMap();
     });
   } 
    
//load map function
loadMap() {
    this.map = new GoogleMap(
      //properties to be present on the map 
      'map', {
              'backgroundColor': 'white',
              'controls': {
              'compass': true,
              'myLocationButton': true,
              'indoorPicker': true,
              'zoom': true,
            },
            'gesture':{
              'scroll': true,
              'tilt': true,
              'rotate': true,
              'zoom': true
            },
            'camera': {
              'latLng': this.latLng,
              'tilt': 30,
              'zoom': 15,
              'bearing': 50
            }
      });
      this.map.on(GoogleMapsEvent.MAP_READY).subscribe(() => {
        console.log('Map Ready');
        this.setMarker(); //set marker when map is ready
      });
 }

 setMarker(){
   if(this.latLng){
     //can add
     //custom .png
     //marker here
     //let customMarker = "www/assets/custom-marker.png";

     //default marker
     let markerOptions: MarkerOptions = {
        position: this.latLng,
        title: 'My Location'
        //icon: customMarker; for custom marker
     };

     this.map.addMarker(markerOptions)
     .then((marker: Marker) =>{
        marker.showInfoWindow();
     });
   }
  }
}